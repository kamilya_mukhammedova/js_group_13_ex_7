export class Item {
  isClicked = false;
  totalPrice = 0;
  constructor(
    public name: string,
    public price: number,
    public quantity: number,
    public image: string,
  ) {}
}
