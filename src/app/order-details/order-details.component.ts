import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent {
  @Input() orderItemName = '';
  @Input() orderItemPrice = 0;
  @Input() orderItemQuantity = 0;
  @Input() isClickedItem = false;
  @Output() deletedItem = new EventEmitter();

  deleteItem() {
    this.deletedItem.emit();
  }
}
