import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent {
  @Input() name = '';
  @Input() price = 0;
  @Input() image = '';
  @Input() imageName = '';
  @Output() clickedItem = new EventEmitter();

  clickOnItem() {
    this.clickedItem.emit();
  }
}
